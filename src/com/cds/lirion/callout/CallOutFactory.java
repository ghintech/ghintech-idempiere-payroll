package com.cds.lirion.callout;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;
import org.compiere.model.CalloutEngine;
import org.eevolution.model.MHRAttribute;

public class CallOutFactory  extends CalloutEngine implements IColumnCalloutFactory{

	@Override
	public IColumnCallout[] getColumnCallouts(String tableName, String columnName) {

		if(tableName.equals(MHRAttribute.Table_Name)){
			if (columnName.equalsIgnoreCase(MHRAttribute.COLUMNNAME_HR_Concept_ID)) {
				return new IColumnCallout[] {new SetHRAttributeColumnType()};
			}
		}
		return new IColumnCallout[0];
	}	

}
