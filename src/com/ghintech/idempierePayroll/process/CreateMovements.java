package com.ghintech.idempierePayroll.process;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

import org.compiere.model.MBPartner;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.compiere.util.TimeUtil;
import org.eevolution.model.MHRConcept;
import org.eevolution.model.MHREmployee;
import org.eevolution.model.MHRMovement;
import org.eevolution.model.MHRPayroll;
import org.eevolution.model.MHRPayrollConcept;
import org.eevolution.model.MHRPeriod;
import org.eevolution.model.MHRProcess;
import org.eevolution.model.MHRProcess.Map;

public class CreateMovements extends SvrProcess{
	private int HR_Process_ID;
	private boolean IsPayrollApplicableToEmployee = false;
	private HashMap<String, Object> m_scriptCtx = new HashMap<String, Object>();
	private int m_C_BPartner_ID = 0;
	private int m_HR_Concept_ID = 0;
	private Timestamp m_dateFrom;
	private Timestamp m_dateTo;
	private MHRPayrollConcept[] linesConcept;
	private MHREmployee m_employee;
	private Hashtable<Integer, MHRMovement> m_movement = new Hashtable<Integer, MHRMovement>();
	private List<Map> m_movement2 = new ArrayList<Map>();

	@Override
	protected void prepare() {
		// TODO Auto-generated method stub
		HR_Process_ID=this.getRecord_ID();
	}

	@Override
	protected String doIt() throws Exception {
		// TODO Auto-generated method stub
		createMovements();
		return null;
	}
	private void createMovements() throws Exception {
		// Valid if used definition of payroll per employee > ocurieles
		// 18Nov2014
		MHRProcess HRProcess= new MHRProcess(getCtx(), HR_Process_ID, get_TrxName());
		MHRPayroll Payroll = MHRPayroll.get(Env.getCtx(), HRProcess.getHR_Payroll_ID());
		boolean includeInActiveEmployee = Payroll.get_ValueAsBoolean("HR_IncludeInActiveEmployee");
		boolean allOrg = Payroll.get_ValueAsBoolean("HR_AllOrg");
		if (Payroll != null || !Payroll.equals(null))
			IsPayrollApplicableToEmployee = Payroll
			.get_ValueAsBoolean("IsemployeeApplicable");

		m_scriptCtx.clear();
		m_scriptCtx.put("process", this);
		m_scriptCtx.put("_Process", HRProcess.getHR_Process_ID());
		m_scriptCtx.put("_Period", HRProcess.getHR_Period_ID());
		m_scriptCtx.put("_Payroll", HRProcess.getHR_Payroll_ID());
		m_scriptCtx.put("_Department", HRProcess.getHR_Department_ID());

		log.info("info data - " + " Process: " + HRProcess.getHR_Process_ID()
		+ ", Period: " + HRProcess.getHR_Period_ID() + ", Payroll: "
		+ HRProcess.getHR_Payroll_ID() + ", Department: " + HRProcess.getHR_Department_ID());
		MHRPeriod period = new MHRPeriod(getCtx(), HRProcess.getHR_Period_ID(),
				get_TrxName());
		if (period != null) {
			m_dateFrom = period.getStartDate();
			m_dateTo = period.getEndDate();
			m_scriptCtx.put("_From", period.getStartDate());
			m_scriptCtx.put("_To", period.getEndDate());
		}

		// RE-Process, delete movement except concept type Incidence
		int no = DB
				.executeUpdateEx(
						"DELETE FROM HR_Movement m WHERE HR_Process_ID=? AND IsRegistered<>?",
						new Object[] { HRProcess.getHR_Process_ID(), true },
						get_TrxName());
		log.info("HR_Movement deleted #" + no);
		MBPartner[] linesEmployee =null;
		if (includeInActiveEmployee){
			linesEmployee = MHREmployee.getEmployeesAll(HRProcess,allOrg);
		}else{
			linesEmployee = MHREmployee.getEmployees(HRProcess);
		}
		linesConcept = MHRPayrollConcept.getPayrollConcepts(HRProcess);

		//
		int count = 1;
		for (MBPartner bp : linesEmployee) // ===============================================================
			// Employee
		{
			statusUpdate("Calculando empleado "+ count+ " de "+linesEmployee.length +":"+bp.getName());
			if(HRProcess.isOnVacation(bp.getC_BPartner_ID()))
				continue;
			log.info("Employee " + count + "  ---------------------- "
					+ bp.getName());
			count++;
			m_C_BPartner_ID = bp.get_ID();
			if (HRProcess.getHR_Payroll_ID() > 0 && IsPayrollApplicableToEmployee && !allOrg){
				if (includeInActiveEmployee){
					m_employee = MHREmployee.getEmployee(getCtx(),
							m_C_BPartner_ID, HRProcess.getAD_Org_ID(), get_TrxName(),
							HRProcess.getHR_Payroll_ID());
				}else{
					m_employee = MHREmployee.getActiveEmployee(getCtx(),
							m_C_BPartner_ID, HRProcess.getAD_Org_ID(), get_TrxName(),
							HRProcess.getHR_Payroll_ID());
				}				
			}

			else{
				if (includeInActiveEmployee){
					m_employee = MHREmployee.getEmployee(getCtx(),
							m_C_BPartner_ID, get_TrxName());
				}else{
					m_employee = MHREmployee.getActiveEmployee(getCtx(),
							m_C_BPartner_ID, get_TrxName());	
				}

			}

			m_scriptCtx.remove("_DateStart");
			m_scriptCtx.remove("_DateEnd");
			m_scriptCtx.remove("_Days");
			m_scriptCtx.remove("_C_BPartner_ID");
			m_scriptCtx.put("_DateStart", m_employee.getStartDate());
			m_scriptCtx.put(
					"_DateEnd",
					m_employee.getEndDate() == null ? TimeUtil.getDay(2999, 12,
							31) : m_employee.getEndDate());
			m_scriptCtx.put(
					"_Days",
					org.compiere.util.TimeUtil.getDaysBetween(
							period.getStartDate(), period.getEndDate()) + 1);
			m_scriptCtx.put("_C_BPartner_ID", bp.getC_BPartner_ID());
			m_scriptCtx.put("_JobEmployee", m_employee.getHR_Job_ID());

			m_movement.clear();
			HRProcess.loadMovements(m_movement, m_C_BPartner_ID);
			for (MHRPayrollConcept pc : linesConcept) // ====================================================
				// Concept
			{
				//System.out.println(increment++);
				//System.out.println(pc.getHR_Concept().getName());
				m_HR_Concept_ID = pc.getHR_Concept_ID();
				MHRConcept concept = MHRConcept.get(getCtx(), m_HR_Concept_ID);

				boolean printed = pc.isPrinted() || concept.isPrinted();
				//Boolean byDate = concept.get_ValueAsBoolean("IsApplyByDate");

				/*if (byDate) {
					Timestamp[] dates = HRProcess.getDates2(period.getStartDate(), period.getEndDate());
					m_scriptCtx.put("_From", dates[0]);
					m_scriptCtx.put("_To", dates[1]);
				}*/

				MHRMovement movement = m_movement.get(concept.get_ID()); // as
				// it's
				// now
				// recursive,
				// it
				// canappen
				// that
				// the
				// concept
				// is
				// already
				// generated
				if (movement == null) {

					movement = HRProcess.createMovementFromConcept(concept, printed);
					movement = m_movement.get(concept.get_ID());
					m_scriptCtx.put("_From", period.getStartDate());
					m_scriptCtx.put("_To", period.getEndDate());
				}
				if (movement == null) {
					continue;
					//throw new AdempiereException("Concept "
					//							+ concept.getValue() + " not created");
				}
				//for(MHRMovement movement : listmovement)
				movement.set_ValueOfColumn("SeqNo", pc.getSeqNo());
			} // concept

			// Save movements:
			for (MHRMovement m : m_movement.values()) {
				MHRConcept c = (MHRConcept) m.getHR_Concept();
				if ((c.isRegistered() || m.isEmpty())
						&& !c.get_ValueAsBoolean("HR_MovementInsertForce")) {
					log.fine("Skip saving " + m);
				} else {
					boolean saveThisRecord = m.isPrinted() || c.isPaid()
							|| c.isPrinted();
					if (saveThisRecord)
						m.saveEx();
				}
			}
			for (Map m2 : m_movement2) {
				MHRMovement m = m2.getMov();
				MHRConcept c = (MHRConcept) m.getHR_Concept();
				if ((c.isRegistered() || m.isEmpty())
						&& !c.get_ValueAsBoolean("HR_MovementInsertForce")) {
					log.fine("Skip saving " + m);
				} else {
					boolean saveThisRecord = m.isPrinted() || c.isPaid()
							|| c.isPrinted();
					if (saveThisRecord)
						m.saveEx();
				}
			}
			//setLastPayroll(bp.getC_BPartner_ID());
		} // for each employee
		//
		// Save period & finish
		period.setProcessed(true);
		period.saveEx();
	} // createMovements
}
