package com.ghintech.component;

import org.adempiere.base.IProcessFactory;
import org.compiere.process.ProcessCall;
import org.eevolution.process.HRCreatePeriods;
import org.ghintech.process.ImportEmployee;
import org.ghintech.process.importPayrollFromTxt;

import com.ghintech.idempierePayroll.process.CreateMovements;

public class GhintechProcessFactory implements IProcessFactory{

	@Override
	public ProcessCall newProcessInstance(String className) {
		if(className.equals("org.eevolution.process.HRCreatePeriods"))
			return new HRCreatePeriods();
		if(className.equals("org.ghintech.process.ImportEmployee"))
			return new ImportEmployee();
		if(className.equals("com.ghintech.process.CreateMovements"))
			return new CreateMovements();
		if(className.equals("org.ghintech.process.importPayrollFromTxt"))
			return new importPayrollFromTxt();
		return null;
	}

}
