package org.ghintech.process;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.adempiere.exceptions.AdempiereException;
import org.compiere.model.MAttachment;
import org.compiere.model.MAttachmentEntry;
import org.compiere.model.MBPartner;
import org.compiere.model.Query;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.compiere.util.Env;
import org.eevolution.model.MHRConcept;
import org.eevolution.model.MHRMovement;
import org.eevolution.model.MHRPayroll;
import org.eevolution.model.MHRPeriod;
import org.eevolution.model.MHRProcess;
import org.eevolution.model.X_HR_PayrollConcept;
/****
 *	Programa para leer txt del tacp
 *	estructura de las columnas
 *	1-23 nombre y apellido
 *	23-34 cedula
 *		23 provincia
 *		26-29 tomo
 *		30-34 partida
 *		35 Genero
 *		36-52 Nro seguro social
 *		53-59 salario
 *			54-58	parte entera
 *			58-60	parte decimal
 *		62-66	Monto seguro social
 *			62-64	parte entera
 *			65-66	parte decimal
 *		72-76	ISLR
 *			72-44	parte entera
 *			75-76	parte decimal
 *		79-82	Monto seguro social
 *			79-80	parte entera
 *			81-82	parte decimal
 *		116-121	Monto a pagar
 *			116-119	parte entera
 *			120-121	parte decimal
 * @author alara
 *
 */
public class importPayrollFromTxt extends SvrProcess{

	//Variables para conceptos de planilla
	private List<String> HR_ConceptList = new ArrayList<String>();
	static String endLineChar = "*";
	MHRProcess process;
	MHRPeriod period;
	MHRPayroll payroll;
	@Override
	protected void prepare() {
		HR_ConceptList.add("PA_CC_SUELDO");
		HR_ConceptList.add("PA_CC_ISLR");
		HR_ConceptList.add("PA_CC_SEGURO_SOCIAL_EMP");
		HR_ConceptList.add("PA_CC_SEGURO_EDUCATIVO_EMP");
		HR_ConceptList.add("PA_CC_PRESTAMO_ACREEDOR_EMP");
		HR_ConceptList.add("PA_CC_SIACAP");
		
	}

	@Override
	protected String doIt() throws Exception {
		

		
		process = new MHRProcess(getCtx(), getRecord_ID(), get_TrxName());
		period = new MHRPeriod(getCtx(),process.getHR_Period_ID(),get_TrxName());
		payroll = new MHRPayroll(getCtx(), process.getHR_Payroll_ID(), get_TrxName());
		
		MAttachment attachment = process.getAttachment();
		if (attachment == null) {
			return "@Error@Please attach the txt file before running the process";
		}
		
		MAttachmentEntry entry = attachment.getEntry(0);
		if (entry == null) {
			return "@Error@Please attach the attendance file before running the process";
		}
		StringBuilder clientCheck = new StringBuilder(" AND AD_Client_ID=").append(getAD_Client_ID());
		StringBuilder sql = new StringBuilder ("DELETE HR_Movement ")
				.append("WHERE HR_Process_ID=").append(process.get_ID()).append (clientCheck);
		int	no = DB.executeUpdate(sql.toString(), get_TrxName());
		
		File csvFile = entry.getFile();
		FileReader fileReader = new FileReader(csvFile);
		int countlines=0;
		
		try (BufferedReader br = new BufferedReader(fileReader)) {
			
			//Remove file header: static first 8 lines of the file
			String inputLine;
			List<String> lineList = new ArrayList<String>();
			while ((inputLine = br.readLine()) != null) {
				if(countlines<8) {
					countlines++;
					continue;					
				}
				if(inputLine.length()==0)
					break;
				lineList.add(inputLine);
				countlines++;
			}
			fileReader.close();
			StringBuilder taxid=new StringBuilder();
			String oldtaxid = "";
			BigDecimal sueldoAmt=Env.ZERO;
			BigDecimal ssAmt=Env.ZERO;
			BigDecimal islrAmt=Env.ZERO;
			BigDecimal seAmt=Env.ZERO;
			BigDecimal siacapAmt=Env.ZERO;
			BigDecimal pAmt=Env.ZERO;
			BigDecimal totalAmt=Env.ZERO;
			for (String line : lineList) {	
				if(line.trim().length()==0)
					break;
				if(line.length()<82)
					continue;			
				
				if(line.substring(23, 24).trim().length()!=0) {
					taxid=new StringBuilder();
					taxid.append(line.substring(23, 24));
					taxid.append("-");
					taxid.append(line.substring(26, 30).replaceFirst("^0+", ""));
					taxid.append("-");
					taxid.append(line.substring(30, 35).replaceFirst("^0+", ""));
					System.out.println(taxid);
					
					if(oldtaxid.compareTo(taxid.toString())!=0 && oldtaxid.length()>0 && taxid.toString().length()>0) {
						pAmt=sueldoAmt.subtract(ssAmt).subtract(islrAmt).subtract(seAmt).subtract(siacapAmt).subtract(totalAmt);				
						//Creando movimientos de planilla						
						for (String HR_Concept : HR_ConceptList) {
							BigDecimal amt = Env.ZERO;
							if(HR_Concept.compareTo("PA_CC_SUELDO")==0)
								amt = sueldoAmt;
							else if(HR_Concept.compareTo("PA_CC_ISLR")==0)
								amt = islrAmt;
							else if(HR_Concept.compareTo("PA_CC_SEGURO_SOCIAL_EMP")==0)
								amt = ssAmt;
							else if(HR_Concept.compareTo("PA_CC_SEGURO_EDUCATIVO_EMP")==0)
								amt = seAmt;
							else if(HR_Concept.compareTo("PA_CC_PRESTAMO_ACREEDOR_EMP")==0)
								amt = pAmt;
							else if(HR_Concept.compareTo("PA_CC_SIACAP")==0)
								amt = siacapAmt;														
							createMovement(HR_Concept, amt, oldtaxid);
						}						
						sueldoAmt=Env.ZERO;
						ssAmt=Env.ZERO;
						islrAmt=Env.ZERO;
						seAmt=Env.ZERO;
						pAmt=Env.ZERO;
						totalAmt=Env.ZERO;
					}
					
					if(taxid.toString().trim().length()!=0) {
						sueldoAmt = new BigDecimal(line.substring(53,57).trim());
						sueldoAmt = sueldoAmt.add(new BigDecimal("0.".concat(line.substring(57,59))));
						ssAmt = new BigDecimal(line.substring(61,64).trim());
						ssAmt = ssAmt.add(new BigDecimal("0.".concat(line.substring(64,66))));
						islrAmt = new BigDecimal(line.substring(71,74).trim());
						islrAmt = islrAmt.add(new BigDecimal("0.".concat(line.substring(74,76))));
						seAmt = new BigDecimal(line.substring(78,80).trim());
						seAmt = seAmt.add(new BigDecimal("0.".concat(line.substring(80,82))));
						if(line.substring(86,88).trim().length()!=0) {
							siacapAmt = new BigDecimal(line.substring(86,88).trim());
							siacapAmt = siacapAmt.add(new BigDecimal("0.".concat(line.substring(88,90))));	
						}
					}
				}
				if(line.length()>122)
					if(line.substring(119,121).trim().length()!=0) {
						//if(line.substring(line.length()-1).compareTo(endLineChar)==0) {
							if(line.substring(115,119).trim().length()!=0) {
								totalAmt = new BigDecimal(line.substring(115,119).trim());
								totalAmt = totalAmt.add(new BigDecimal("0.".concat(line.substring(119,121))));
							}
						}
			oldtaxid=taxid.toString();
			}
			
			if(oldtaxid.compareTo(taxid.toString())==0 && oldtaxid.length()>0 && taxid.toString().length()>0) {
				pAmt=sueldoAmt.subtract(ssAmt).subtract(islrAmt).subtract(seAmt).subtract(siacapAmt).subtract(totalAmt);				
				//Creando movimientos de planilla						
				for (String HR_Concept : HR_ConceptList) {
					BigDecimal amt = Env.ZERO;
					if(HR_Concept.compareTo("PA_CC_SUELDO")==0)
						amt = sueldoAmt;
					else if(HR_Concept.compareTo("PA_CC_ISLR")==0)
						amt = islrAmt;
					else if(HR_Concept.compareTo("PA_CC_SEGURO_SOCIAL_EMP")==0)
						amt = ssAmt;
					else if(HR_Concept.compareTo("PA_CC_SEGURO_EDUCATIVO_EMP")==0)
						amt = seAmt;
					else if(HR_Concept.compareTo("PA_CC_PRESTAMO_ACREEDOR_EMP")==0)
						amt = pAmt;
					else if(HR_Concept.compareTo("PA_CC_SIACAP")==0)
						amt = siacapAmt;														
					createMovement(HR_Concept, amt, oldtaxid);
				}						
				sueldoAmt=Env.ZERO;
				ssAmt=Env.ZERO;
				islrAmt=Env.ZERO;
				seAmt=Env.ZERO;
				pAmt=Env.ZERO;
				totalAmt=Env.ZERO;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return null;
	}

	
	private boolean createMovement(String concept,BigDecimal Amt,String taxid) {
		boolean success = false;
		MBPartner bp = new Query(getCtx(), MBPartner.Table_Name, "taxid='"+taxid+"'", get_TrxName()).first();
		if(bp==null) {
			log.warning("TE FALTA INGRESAR LA CEDULA CON EL FORMATO CORRECTO");
			return false;
			}		
		if(Amt.compareTo(Env.ZERO)==0) {
			return false;
		}
			
		MHRMovement HRmovement = new MHRMovement(getCtx(), 0, get_TrxName());
		HRmovement.setHR_Process_ID(process.get_ID());
		MHRConcept hrconcept = new Query(getCtx(), MHRConcept.Table_Name, "Value='"+concept+"'", get_TrxName()).first();
		X_HR_PayrollConcept pc = new Query(getCtx(), X_HR_PayrollConcept.Table_Name, "HR_Concept_ID="+hrconcept.get_ID(), get_TrxName()).first();
		if(pc==null)
			throw new AdempiereException("El concepto: "+hrconcept.getName()+" no esta agregado a la planilla");		
		HRmovement.setHR_Concept_Category_ID(hrconcept.getHR_Concept_Category_ID());
		HRmovement.setHR_Concept_ID(hrconcept.get_ID());		
		HRmovement.setC_BPartner_ID(bp.get_ID());
		HRmovement.setColumnType(hrconcept.getColumnType());
		HRmovement.setAccountSign(hrconcept.getAccountSign());
		HRmovement.setAmount(Amt);
		HRmovement.setValidFrom(period.getStartDate());
		HRmovement.setValidTo(period.getEndDate());
		HRmovement.setIsPrinted(pc.isPrinted());
		HRmovement.saveEx();
		success= true;
		
		return success;
	}
}
