package org.ghintech.process;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;

import org.compiere.model.MBPartner;
import org.compiere.model.MLocation;
import org.compiere.model.MUser;
import org.compiere.model.Query;
import org.compiere.model.X_C_BP_BankAccount;
import org.compiere.model.X_C_BPartner_Location;
import org.compiere.process.ProcessInfoParameter;
import org.compiere.process.SvrProcess;
import org.compiere.util.DB;
import org.eevolution.model.X_HR_Employee;
import org.eevolution.model.X_HR_Job;
import org.eevolution.model.X_I_HR_Employee;
import org.eevolution.model.X_I_HR_Movement;

public class ImportEmployee extends SvrProcess{

	boolean p_DeleteOldImported=false;
	@Override
	protected void prepare() {
		ProcessInfoParameter[] para = getParameter();
		for (int i = 0; i < para.length; i++)
		{
			String name = para[i].getParameterName();
			if (name.equals("DeleteOldImported"))
				p_DeleteOldImported = "Y".equals(para[i].getParameter());
			else
				log.log(Level.SEVERE, "Unknown Parameter: " + name);
		}
		
	}

	@Override
	protected String doIt() throws Exception {
		StringBuffer sql = null;
		int no = 0;
		String clientCheck = " AND AD_Client_ID=" + this.getAD_Client_ID();
		
		//	****	Prepare	****

		//	Delete Old Imported
		if (p_DeleteOldImported)
		{
			sql = new StringBuffer ("DELETE I_HR_Employee "
				+ "WHERE I_IsImported='Y'").append(clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			log.info("Delete Old Imported =" + no);
		}
		
		//Asignar valores preexistentes
		//	Set Supervisor_ID
		sql = new StringBuffer ("UPDATE I_HR_Employee "
			+ "SET Supervisor_ID = (SELECT AD_User_ID FROM AD_User WHERE Name=I_HR_Employee.SupervisorName "+clientCheck+" FETCH FIRST ROW ONLY) "
			+ "WHERE (I_IsImported<>'Y' OR I_IsImported IS NULL) AND Supervisor_ID IS NOT NULL "+clientCheck);
		no = DB.executeUpdate(sql.toString(), get_TrxName());
		log.info("Supervisor_ID=" + no);
		//	Set HR_Job_ID
		sql = new StringBuffer ("UPDATE I_HR_Employee "
				+ "SET HR_Job_ID = (SELECT HR_Job_ID FROM HR_Job WHERE Name=I_HR_Employee.HR_Job_Name  "+clientCheck+" FETCH FIRST ROW ONLY) "
				+ "WHERE (I_IsImported<>'Y' OR I_IsImported IS NULL) AND HR_Job_ID IS NULL "+clientCheck);
		no = DB.executeUpdate(sql.toString(), 	get_TrxName());
		log.info("HR_Job_ID=" + no);
		
		//Set C_BPartner_ID if exists
		sql = new StringBuffer ("UPDATE I_HR_Employee "
				+ "SET C_BPartner_ID = (SELECT C_BPartner_ID FROM C_BPartner WHERE value=I_HR_Employee.Value "+clientCheck+" FETCH FIRST ROW ONLY ) "
				+ "WHERE (I_IsImported<>'Y' OR I_IsImported IS NULL) AND C_BPartner_ID IS NULL "+clientCheck);
		no = DB.executeUpdate(sql.toString(), 	get_TrxName());
		log.warning("C_BPartner_ID=" + no);
		
		//Validate HR_Department_ID
		sql = new StringBuffer ("UPDATE I_HR_Employee "
				+ "SET I_IsImported='E',I_ErrorMsg='No HR_Department' "
				+ "WHERE HR_Department_ID IS NULL AND (I_IsImported<>'Y' OR I_IsImported IS NULL) "+clientCheck);
		no = DB.executeUpdate(sql.toString(), 	get_TrxName());
		log.warning("HR_Department_ID=" + no);
		
		commitEx();
		int noInserthrm = 0;
		int noUpdatehrm = 0;
		//	Go through Records
		log.fine("start inserting/updating ...");
		
		sql = new StringBuffer ("SELECT * FROM I_HR_Employee WHERE I_IsImported='N'")
				.append(clientCheck);
			PreparedStatement pstmt_setImported = null;
			PreparedStatement pstmt = null;
			ResultSet rs = null;
			try
			{
				//	Set Imported = Y
				pstmt_setImported = DB.prepareStatement
					("UPDATE I_HR_Employee SET I_IsImported='Y', C_BPartner_ID=?, "
					+ "Updated=SysDate, Processed='Y' WHERE I_HR_Employee_ID=?", get_TrxName());

				//
				pstmt = DB.prepareStatement(sql.toString(), get_TrxName());
				rs = pstmt.executeQuery();
				while (rs.next())
				{
					X_I_HR_Employee ie = new X_I_HR_Employee(getCtx(), rs, get_TrxName());
					int HR_Employee_ID = ie.getI_HR_Employee_ID();
					MBPartner bp = null;
					//if bpartner exists
					if(ie.getC_BPartner_ID()>0) {
						bp = new MBPartner(getCtx(), ie.getC_BPartner_ID(), get_TrxName());						
					}
						
					else {
						//Create business partner
						bp = new MBPartner(getCtx(), 0, get_TrxName());
						bp.setName(ie.getName());
						bp.setTaxID(ie.getTaxID());
						bp.setC_BP_Group_ID(ie.getC_BP_Group_ID());
						bp.setAD_Org_ID(ie.getAD_Org_ID());
						bp.setDescription(ie.getDescription());
						bp.setValue(ie.getValue());
					}
					
					bp.setIsEmployee(ie.isEmployee());
					bp.setIsSalesRep(ie.isSalesRep());					
					bp.saveEx();
										
					//Create/Update User
					MUser[] ulist =MUser.getOfBPartner(getCtx(), bp.get_ID(), get_TrxName());
					MUser u = null;
					if(ulist.length>0)
						u= ulist[0];
					else
						u=new MUser(getCtx(), 0, get_TrxName());
					
					log.warning(ulist.length+"*********");
					u.setAD_Org_ID(ie.getAD_Org_ID());
					u.setC_BPartner_ID(bp.getC_BPartner_ID());
					u.setName(ie.getName());
					u.setEMail(ie.getEMail());
					u.setPhone(ie.getPhone());
					u.setPhone2(ie.getPhone2());
					u.setIsInPayroll(ie.isInPayroll());
					u.setBirthday(ie.getBirthday());
					u.set_ValueOfColumn("BloodType",ie.get_ValueAsString("BloodType"));
					u.set_ValueOfColumn("MaritalStatus", ie.get_Value("MaritalStatus"));
					u.saveEx();
					
					//Create/update employee
					X_HR_Employee emp = null;
							
					emp = new Query(getCtx(), X_HR_Employee.Table_Name, "C_BPartner_ID = ?", get_TrxName()).setParameters(bp.get_ID()).first();
					if(emp==null)
						emp = new X_HR_Employee(getCtx(), 0, get_TrxName());
					
					emp.setNationalCode(ie.getNationalCode());
					emp.setC_BPartner_ID(bp.get_ID());
					emp.setAD_Org_ID(ie.getAD_Org_ID());
					emp.setSSCode(ie.getSSCode());
					emp.setStartDate(ie.getStartDate());
					emp.setEndDate(ie.getEndDate());
					emp.setHR_Department_ID(ie.getHR_Department_ID());
					if(ie.getHR_Job_ID()>0)
						emp.setHR_Job_ID(ie.getHR_Job_ID());
					else {
						//create job
						X_HR_Job job = new X_HR_Job(getCtx(), 0, get_TrxName());
						job.setName(ie.get_ValueAsString("HR_Job_Name"));
						job.saveEx();
						ie.setHR_Job_ID(job.get_ID());
						emp.setHR_Job_ID(ie.getHR_Job_ID());
					}
					emp.setHR_Payroll_ID(ie.getHR_Payroll_ID());
					emp.saveEx();	
					noInserthrm++;
					noUpdatehrm++;
					HR_Employee_ID = emp.getHR_Employee_ID();
					//Create/Update Bank Info
					
					X_C_BP_BankAccount bpacct = new X_C_BP_BankAccount(getCtx(), 0, get_TrxName());
					bpacct.setAD_Org_ID(ie.getAD_Org_ID());
					bpacct.setIsACH(ie.isACH());
					bpacct.setC_BPartner_ID(bp.get_ID());
					bpacct.setBPBankAcctUse(ie.getBPBankAcctUse());
					bpacct.setC_Bank_ID(ie.getC_Bank_ID());
					bpacct.setBankAccountType(ie.getBankAccountType());
					bpacct.setRoutingNo(ie.getRoutingNo());
					bpacct.setAccountNo(ie.getAccountNo());
					bpacct.setA_Name("-");
					bpacct.saveEx();
					//Create Location
					if(ie.getAddress1()!=null) {
						X_C_BPartner_Location bpl = new X_C_BPartner_Location(getCtx(), 0, get_TrxName());
						MLocation l = new MLocation(getCtx(), ie.getC_Country_ID(), ie.getC_Region_ID(),ie.getCity(), get_TrxName());
						l.setAddress1(ie.getAddress1());
						l.setAddress2(ie.getAddress2());
						l.saveEx();
						
						bpl.setC_Location_ID(l.get_ID());
						bpl.setName(ie.getAddress1());
						bpl.setC_BPartner_ID(bp.get_ID());
						bpl.saveEx();
					}
					//	Update I_HR_Movement
					pstmt_setImported.setInt(1, bp.get_ID());
					pstmt_setImported.setInt(2, ie.get_ID());
					no = pstmt_setImported.executeUpdate();
					//
					commitEx();
				}
			}
			catch (SQLException e)
			{
				throw e;
			}
			finally
			{
				DB.close(rs, pstmt);
				rs = null;
				pstmt = null;
				DB.close(pstmt_setImported);
				pstmt_setImported = null;
			}
			//	Set Error to indicator to not imported
			sql = new StringBuffer ("UPDATE I_HR_Employee "
				+ "SET I_IsImported='N', Updated=SysDate "
				+ "WHERE I_IsImported<>'Y'").append(clientCheck);
			no = DB.executeUpdate(sql.toString(), get_TrxName());
			addLog (0, null, new BigDecimal (no), "@Errors@");
			addLog (0, null, new BigDecimal (noInserthrm), "@HR_Employee_ID@: @Inserted@");
			addLog (0, null, new BigDecimal (noUpdatehrm), "@HR_Employee_ID@: @Updated@");
			return "";
	}

}
